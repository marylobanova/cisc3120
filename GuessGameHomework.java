import java.lang.Math;
import java.util.Random;
import java.util.Scanner;

public class GuessGameHomework {
    public static void main(String[] args) {

        // This is how you convert a string to an integer:
        int maxInteger = 16; // Integer.parseInt(args[0]);
        // This is the number of guesses the user gets.
        int limit = 4; // Integer.parseInt(args[1]);
        // This creates a new random number generator
        Random rand = new Random();
        // This generates a random integer.  Note the "+1"!
        int target = rand.nextInt(maxInteger);
        // This scans input from stdin.
        // (Scanner is a little like cin in C++)
        Scanner input = new Scanner(System.in);
        // This is how you print to stdout.
        System.out.printf("Guess a number between 1 and %d.\n", maxInteger);
        int guess = 0;
        //list of previously guessed numbers
        int[] attemptsValues = new int[limit];
        
        //continue the game until maximum number 
        //of tries is reached
        int attempts = 1;
        try {  
            while (attempts <= limit) {
                System.out.printf("Attempt %d of %d: ", attempts, limit);
                try {
                    guess = input.nextInt();
                    while (!(checkBounds(guess, maxInteger)) || !(checkRedundancy(guess, limit, attemptsValues))){
                        //check if guessed number is out of bounds
                        if (!(checkBounds(guess, maxInteger))){
                            System.out.print ("Number "+ guess + " is out of bounds! Try again: ");
                            guess = input.nextInt();
                        }
                        //check if guessed number was already selected
                        else if (!(checkRedundancy(guess, limit, attemptsValues))){
                            System.out.print ("Number " + guess + " was already tried before. Try again: ");
                            guess = input.nextInt();
                        }
                    } 
                } catch (java.util.InputMismatchException e){
                    System.out.println("Not a number!");
                    input.nextLine();
                    continue;
                } 
                //save guessed number
                attemptsValues[attempts-1] = guess;
                attempts+=1;

                System.out.printf("You guessed %d\n", guess);
                //guessed number is greater than target
                if (guess > target)
                    System.out.println("Too high!");
                //guessed number is less than target
                else if (guess < target)
                    System.out.println("Too low!");
                //guessed number is the target
                else {
                    System.out.println("You won!");
                    System.exit(0);
                }        
            } 
            System.out.println("You lose!");

        } finally {
                System.out.println("Game is over!");
                input.close();  
        }      
    }

    //checks if guessed number is greater than 0
    //and less than defined max
    public static boolean checkBounds(int g, int m)
    {
        if ((g < 1) || (g > m))
            return false;
        return true;
    }

    //checks if guessed number was already selected
    public static boolean checkRedundancy (int g, int l, int[] a)
    {
        for (int i = 0; i < l; i++){
            if (g == a[i])
                return false;
        }
        return true;
    }
}

