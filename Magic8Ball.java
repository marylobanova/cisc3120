package edu.cuny.brooklyn.cisc3120.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class Magic8Ball
{
    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    static String[] answers = {
        "Defiantly.",
        "No way José.",
        "Most likely.",
        "Outlook bad.",
        "Signs point to yes.",
        "Yes.",
        "You would rather not know, trust me!",
	    "Does not compute."
    };

    public static void main (String[] args) throws IOException
    {
        // Create a new pseudo-random number generator
        Random rand = new Random();
        String input = null;
        while (true) {
            System.out.println("Ask a question.  (Type quit to quit)");
            input = reader.readLine();
            if ("quit".equals(input)) {
                return;
            }
            // Generate a random number between 0 and 15
            int choice = rand.nextInt(answers.length);
            String answer = answers[choice];
            System.out.println("THE MAGIC 8-BALL SAYS: " + answer);
        }
    }
}
