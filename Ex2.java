public class Ex2 extends Exception
{
      //Parameterless Constructor
      public Ex2() {}

      //Constructor that accepts a message
      public Ex2(String message)
      {
         super(message);
      }
 }