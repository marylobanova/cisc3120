public class Ex1 extends Exception
{
      //Parameterless Constructor
      public Ex1() {}

      //Constructor that accepts a message
      public Ex1(String message)
      {
         super(message);
      }
 }