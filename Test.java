import java.lang.Math;
import java.util.Random;
import java.util.Scanner;

public class Test {


	public static void main (String[] args){
		Ex1 exc1 = new Ex1();
		Ex2 exc2 = new Ex2();
		try {
			System.out.println("1");
			try{
				System.out.println("2");
				throw new Ex1();
			} catch (Ex1 ex){
				System.out.println("3");
				throw new Ex2();
			} catch (Exception ex){
				System.out.println("4");
				throw new Exception();
			} finally {
				System.out.println("5");
			}
		} catch (Ex1 ex) {
			System.out.println("6");
		} catch (Ex2 ex) {
			System.out.println("7");
		} catch (Exception ex) {
			System.out.println("8");
		} finally {
			System.out.println("9");
		}
	}

}

// 1
// 2
// 3
// 7
// 4
// 5
// 9
// 8